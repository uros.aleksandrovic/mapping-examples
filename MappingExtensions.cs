﻿using mapping_example.Dtos;
using mapping_example.Entities;

namespace mapping_example
{
    /// <summary>
    /// Decoupling the logic of mapping into separate extension class
    /// </summary>
    public static class MappingExtensions
    {
        public static PersonDto MapToDto(this Person person)
        {
            return new PersonDto
            {
                Id = person.Id.ToString(),
                LastName = person.LastName,
                FirstName = person.FirstName,
                DateOfCreation = person.CreatedOn.ToLongTimeString()
            };
        }

        public static Person MapToDto(this PersonDto person)
        {
            return new Person
            {
                Id = Guid.Parse(person.Id),
                LastName = person.LastName,
                FirstName = person.FirstName,
                CreatedOn = DateTime.Parse(person.DateOfCreation)
            };
        }

    }
}
