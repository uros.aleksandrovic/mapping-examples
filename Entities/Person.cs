﻿namespace mapping_example.Entities;

public class Person
{
    public Guid Id { get; init; }
    public string FirstName { get; init; }
    public string LastName{ get; init; }
    public DateTime CreatedOn { get; init; }
}
