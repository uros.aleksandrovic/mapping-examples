﻿using mapping_example.Entities;

namespace mapping_example.Dtos;

public class PersonDto
{
    public string Id { get; set; }
    public string DateOfCreation { get; set; }
    public string FirstName { get; init; }
    public string LastName { get; init; }

    public PersonDto() { }

    /// <summary>
    /// Constructor mapping
    /// </summary>
    public PersonDto(Person person)
    {
        Id = person.Id.ToString();
        DateOfCreation = person.CreatedOn.ToLongTimeString();
        FirstName = person.FirstName;
        LastName = person.LastName;
    }

    /// <summary>
    /// Static function mapping
    /// </summary>
    public static PersonDto Map(Person person)
    {
        return new PersonDto
        {
            Id = person.Id.ToString(),
            LastName = person.LastName,
            FirstName = person.FirstName,
            DateOfCreation = person.CreatedOn.ToLongTimeString()
        };
    }

    /// <summary>
    /// Implicit operator mapping (not recomended!)
    /// </summary>
    /// <see cref="https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/user-defined-conversion-operators"/>
    /// <remarks>Hides the usage of casting. Makes code hard to debug</remarks>
    public static implicit operator PersonDto(Person person)
    {
        return Map(person);
    }

    /// <summary>
    /// Coded for debuging purposes
    /// </summary>
    public override string ToString()
    {
        return $"Id: {Id}\nDate Of Creation: {DateOfCreation}\nFirst Name: {FirstName}\n Last Name: {LastName}"; 
    }
}
