﻿// See https://aka.ms/new-console-template for more information
using mapping_example;
using mapping_example.Dtos;
using mapping_example.Entities;

/// <summary>
/// Entity that we want to map in further code.
/// </summary>
var person = new Person
{
    LastName = "Test",
    FirstName = "Test",
    CreatedOn = DateTime.Now,
    Id = Guid.NewGuid(),
};

#region Mapping With object initialization

Console.WriteLine("Object Initialization mapping");
var objectInitializationMapping = new PersonDto
{
    Id = person.Id.ToString(),
    LastName = person.LastName,
    FirstName = person.FirstName,
    DateOfCreation = person.CreatedOn.ToLongTimeString()
};

Console.WriteLine(objectInitializationMapping);
Console.WriteLine("\n");

#endregion


#region Mapping with constructor

Console.WriteLine("Mapping with constructor");
PersonDto constructorMapping = new PersonDto(person);

Console.WriteLine(constructorMapping);
Console.WriteLine("\n");

#endregion

#region Mapping with static method

Console.WriteLine("Mapping with static method");
var staticMethodMapping = PersonDto.Map(person);

var staticList = new List<Person>() { person };
var personDtoList = staticList.Select(PersonDto.Map);

Console.WriteLine(staticMethodMapping);
Console.WriteLine("\n");

#endregion

#region Map with extension methods

Console.WriteLine("Map with extension methods");
var extensionMethods = PersonDto.Map(person);

var extensionList = new List<Person>() { person };
var extensionDtoList = extensionList.Select(x => x.MapToDto);

Console.WriteLine(staticMethodMapping);
Console.WriteLine("\n");

#endregion

#region Mapping with implicit operator

Console.WriteLine("Mapping with implicit operator");
PersonDto implicitOperatorMapping = person;
var castedList = extensionList.Select<Person, PersonDto>(x => x);


#endregion